const PouchDB = require('pouchdb');
const db = new PouchDB('http://localhost:5984/kittens');
console.log ("Database created Successfully.");
console.log ("-----------------------------------");
/**
 * displaying info of db
 */
db.info(function(err, info) {
    if (err) {
        return console.log(err);
    } else {
        console.log(info);
        console.log ("-------------db info----------------------");
    }
});

//Preparing the document
doc = {
    _id : '002',
    name: 'astha',
    age : 23,
    designation : 'Developer'
};

//Inserting Document
db.put(doc, function(err, response) {
    if (err) {
        return console.log(err);
    } else {
        console.log("Data added Successfully");
        console.log ("-------------add----------------------");
    }
});

//Reading the contents of a Document
db.get("001", function(err, doc) {
    if (err) {
        return console.log(err);
    } else {
        console.log(doc);
        console.log ("--------------show---------------------");
    }
});

/*//remove
db.remove('001','1-866be501cd978b2a52a2c63e193c031e', function(err) {
    if (err) {
        return console.log(err);
    } else {
        console.log("Data deleted successfully");
        console.log ("-----------------------------------");
    }
});*/

/**
 * del db
 */
/*
db.destroy(function (err, response) {
    if (err) {
        return console.log(err);
    } else {
        console.log('-------->Database Deleted');
    }
});
*/

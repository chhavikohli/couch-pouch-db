const http = require('http');
const request = http.request({
    port: 5984,
    host: '127.0.0.1',
    method: 'PUT',
    path: '/my_db'
});

request.end();
request.on("response",(response) => {
    response.on("end",()=>{
        if(response.statusCode === 201){
            console.log("DB CREATED");
        } else{
            console.log("db failed to create");
        }
    })
});
